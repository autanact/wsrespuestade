
/**
 * WSRespuestaDESkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSRespuestaDESkeleton java skeleton for the axisService
     */
    public class WSRespuestaDESkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSRespuestaDELocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param codigoSolicitud
                                     * @param codigoSistema
                                     * @param estadoExcepcion
                                     * @param estadoPagina
                                     * @param instalacion
                                     * @param codigoDireccion
                                     * @param direccionNormalizada
                                     * @param direccionAnterior
                                     * @param placa
                                     * @param agregado
                                     * @param remanente
                                     * @param latitud
                                     * @param longitud
                                     * @param coordenadaX
                                     * @param coordenadaY
                                     * @param estrato
                                     * @param rural
                                     * @param estadoGeoreferenciacion
                                     * @param codigoDireccionProveevor
                                     * @param codigoPais
                                     * @param codigoDepartamento
                                     * @param codigoMunicipio
                                     * @param codigoComuna
                                     * @param codigoBarrio
                                     * @param codigoLocalizacionTipo1
                                     * @param codigoDaneManzana
                                     * @param codigoPredio
                                     * @param tipoAgregacionNivel1
                                     * @param nombreLocalizacionTipo1
                                     * @param nombreBarrio
                                     * @param nombreComuna
                                     * @param direccionEstandarNLectura
         */
        

                 public co.net.une.www.gis.WSRespuestaDERSType respuestaDE
                  (
                  co.net.une.www.gis.BoundedString15 codigoSolicitud,co.net.une.www.gis.BoundedString30 codigoSistema,co.net.une.www.gis.BoundedString30 estadoExcepcion,co.net.une.www.gis.BoundedString5 estadoPagina,co.net.une.www.gis.BoundedString18 instalacion,co.net.une.www.gis.BoundedString100 codigoDireccion,co.net.une.www.gis.BoundedString250 direccionNormalizada,co.net.une.www.gis.BoundedString250 direccionAnterior,co.net.une.www.gis.BoundedString10 placa,co.net.une.www.gis.BoundedString100 agregado,co.net.une.www.gis.BoundedString100 remanente,java.lang.String latitud,java.lang.String longitud,java.lang.String coordenadaX,java.lang.String coordenadaY,java.math.BigInteger estrato,co.net.une.www.gis.BoundedString2 rural,co.net.une.www.gis.BoundedString1 estadoGeoreferenciacion,co.net.une.www.gis.BoundedString100 codigoDireccionProveevor,co.net.une.www.gis.BoundedString2 codigoPais,co.net.une.www.gis.BoundedString2 codigoDepartamento,co.net.une.www.gis.BoundedString8 codigoMunicipio,co.net.une.www.gis.BoundedString10 codigoComuna,co.net.une.www.gis.BoundedString10 codigoBarrio,co.net.une.www.gis.BoundedString10 codigoLocalizacionTipo1,co.net.une.www.gis.BoundedString14 codigoDaneManzana,co.net.une.www.gis.BoundedString17 codigoPredio,co.net.une.www.gis.BoundedString20 tipoAgregacionNivel1,co.net.une.www.gis.BoundedString100 nombreLocalizacionTipo1,co.net.une.www.gis.BoundedString100 nombreBarrio,co.net.une.www.gis.BoundedString100 nombreComuna,co.net.une.www.gis.BoundedString250 direccionEstandarNLectura
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("codigoSolicitud",codigoSolicitud);params.put("codigoSistema",codigoSistema);params.put("estadoExcepcion",estadoExcepcion);params.put("estadoPagina",estadoPagina);params.put("instalacion",instalacion);params.put("codigoDireccion",codigoDireccion);params.put("direccionNormalizada",direccionNormalizada);params.put("direccionAnterior",direccionAnterior);params.put("placa",placa);params.put("agregado",agregado);params.put("remanente",remanente);params.put("latitud",latitud);params.put("longitud",longitud);params.put("coordenadaX",coordenadaX);params.put("coordenadaY",coordenadaY);params.put("estrato",estrato);params.put("rural",rural);params.put("estadoGeoreferenciacion",estadoGeoreferenciacion);params.put("codigoDireccionProveevor",codigoDireccionProveevor);params.put("codigoPais",codigoPais);params.put("codigoDepartamento",codigoDepartamento);params.put("codigoMunicipio",codigoMunicipio);params.put("codigoComuna",codigoComuna);params.put("codigoBarrio",codigoBarrio);params.put("codigoLocalizacionTipo1",codigoLocalizacionTipo1);params.put("codigoDaneManzana",codigoDaneManzana);params.put("codigoPredio",codigoPredio);params.put("tipoAgregacionNivel1",tipoAgregacionNivel1);params.put("nombreLocalizacionTipo1",nombreLocalizacionTipo1);params.put("nombreBarrio",nombreBarrio);params.put("nombreComuna",nombreComuna);params.put("direccionEstandarNLectura",direccionEstandarNLectura);
		try{
		
			return (co.net.une.www.gis.WSRespuestaDERSType)
			this.makeStructuredRequest(serviceName, "respuestaDE", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    