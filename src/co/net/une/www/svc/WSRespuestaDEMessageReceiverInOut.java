
/**
 * WSRespuestaDEMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */
        package co.net.une.www.svc;

        /**
        *  WSRespuestaDEMessageReceiverInOut message receiver
        */

        public class WSRespuestaDEMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        WSRespuestaDESkeleton skel = (WSRespuestaDESkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJava(op.getName().getLocalPart())) != null)){

        

            if("respuestaDE".equals(methodName)){
                
                co.net.une.www.gis.WSRespuestaDERS wSRespuestaDERS33 = null;
	                        co.net.une.www.gis.WSRespuestaDERQ wrappedParam =
                                                             (co.net.une.www.gis.WSRespuestaDERQ)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    co.net.une.www.gis.WSRespuestaDERQ.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               wSRespuestaDERS33 =
                                                   
                                                   
                                                           wraprespuestaDE(
                                                       
                                                        

                                                        
                                                       skel.respuestaDE(
                                                            
                                                                getCodigoSolicitud(wrappedParam)
                                                            ,
                                                                getCodigoSistema(wrappedParam)
                                                            ,
                                                                getEstadoExcepcion(wrappedParam)
                                                            ,
                                                                getEstadoPagina(wrappedParam)
                                                            ,
                                                                getInstalacion(wrappedParam)
                                                            ,
                                                                getCodigoDireccion(wrappedParam)
                                                            ,
                                                                getDireccionNormalizada(wrappedParam)
                                                            ,
                                                                getDireccionAnterior(wrappedParam)
                                                            ,
                                                                getPlaca(wrappedParam)
                                                            ,
                                                                getAgregado(wrappedParam)
                                                            ,
                                                                getRemanente(wrappedParam)
                                                            ,
                                                                getLatitud(wrappedParam)
                                                            ,
                                                                getLongitud(wrappedParam)
                                                            ,
                                                                getCoordenadaX(wrappedParam)
                                                            ,
                                                                getCoordenadaY(wrappedParam)
                                                            ,
                                                                getEstrato(wrappedParam)
                                                            ,
                                                                getRural(wrappedParam)
                                                            ,
                                                                getEstadoGeoreferenciacion(wrappedParam)
                                                            ,
                                                                getCodigoDireccionProveevor(wrappedParam)
                                                            ,
                                                                getCodigoPais(wrappedParam)
                                                            ,
                                                                getCodigoDepartamento(wrappedParam)
                                                            ,
                                                                getCodigoMunicipio(wrappedParam)
                                                            ,
                                                                getCodigoComuna(wrappedParam)
                                                            ,
                                                                getCodigoBarrio(wrappedParam)
                                                            ,
                                                                getCodigoLocalizacionTipo1(wrappedParam)
                                                            ,
                                                                getCodigoDaneManzana(wrappedParam)
                                                            ,
                                                                getCodigoPredio(wrappedParam)
                                                            ,
                                                                getTipoAgregacionNivel1(wrappedParam)
                                                            ,
                                                                getNombreLocalizacionTipo1(wrappedParam)
                                                            ,
                                                                getNombreBarrio(wrappedParam)
                                                            ,
                                                                getNombreComuna(wrappedParam)
                                                            ,
                                                                getDireccionEstandarNLectura(wrappedParam)
                                                            )
                                                    
                                                         )
                                                     ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), wSRespuestaDERS33, false);
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.gis.WSRespuestaDERQ param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.gis.WSRespuestaDERQ.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.gis.WSRespuestaDERS param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.gis.WSRespuestaDERS.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, co.net.une.www.gis.WSRespuestaDERS param, boolean optimizeContent)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(co.net.une.www.gis.WSRespuestaDERS.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    

                        private co.net.une.www.gis.BoundedString15 getCodigoSolicitud(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getCodigoSolicitud();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString30 getCodigoSistema(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getCodigoSistema();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString30 getEstadoExcepcion(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getEstadoExcepcion();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString5 getEstadoPagina(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getEstadoPagina();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString18 getInstalacion(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getInstalacion();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString100 getCodigoDireccion(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getCodigoDireccion();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString250 getDireccionNormalizada(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getDireccionNormalizada();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString250 getDireccionAnterior(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getDireccionAnterior();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString10 getPlaca(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getPlaca();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString100 getAgregado(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getAgregado();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString100 getRemanente(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getRemanente();
                            
                        }
                     

                        private java.lang.String getLatitud(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getLatitud();
                            
                        }
                     

                        private java.lang.String getLongitud(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getLongitud();
                            
                        }
                     

                        private java.lang.String getCoordenadaX(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getCoordenadaX();
                            
                        }
                     

                        private java.lang.String getCoordenadaY(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getCoordenadaY();
                            
                        }
                     

                        private java.math.BigInteger getEstrato(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getEstrato();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString2 getRural(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getRural();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString1 getEstadoGeoreferenciacion(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getEstadoGeoreferenciacion();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString100 getCodigoDireccionProveevor(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getCodigoDireccionProveevor();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString2 getCodigoPais(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getCodigoPais();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString2 getCodigoDepartamento(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getCodigoDepartamento();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString8 getCodigoMunicipio(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getCodigoMunicipio();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString10 getCodigoComuna(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getCodigoComuna();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString10 getCodigoBarrio(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getCodigoBarrio();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString10 getCodigoLocalizacionTipo1(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getCodigoLocalizacionTipo1();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString14 getCodigoDaneManzana(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getCodigoDaneManzana();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString17 getCodigoPredio(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getCodigoPredio();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString20 getTipoAgregacionNivel1(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getTipoAgregacionNivel1();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString100 getNombreLocalizacionTipo1(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getNombreLocalizacionTipo1();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString100 getNombreBarrio(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getNombreBarrio();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString100 getNombreComuna(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getNombreComuna();
                            
                        }
                     

                        private co.net.une.www.gis.BoundedString250 getDireccionEstandarNLectura(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDERQ().getDireccionEstandarNLectura();
                            
                        }
                     
                        private co.net.une.www.gis.WSRespuestaDERQType getrespuestaDE(
                        co.net.une.www.gis.WSRespuestaDERQ wrappedType){
                            return wrappedType.getWSRespuestaDERQ();
                        }
                        
                        
                    
                         private co.net.une.www.gis.WSRespuestaDERS wraprespuestaDE(
                            co.net.une.www.gis.WSRespuestaDERSType innerType){
                                co.net.une.www.gis.WSRespuestaDERS wrappedElement = new co.net.une.www.gis.WSRespuestaDERS();
                                wrappedElement.setWSRespuestaDERS(innerType);
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (co.net.une.www.gis.WSRespuestaDERQ.class.equals(type)){
                
                           return co.net.une.www.gis.WSRespuestaDERQ.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (co.net.une.www.gis.WSRespuestaDERS.class.equals(type)){
                
                           return co.net.une.www.gis.WSRespuestaDERS.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    