
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */

            package co.net.une.www.svc;
            /**
            *  ExtensionMapper class
            */
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString17".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString17.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "WSRespuestaDE-RS-Type".equals(typeName)){
                   
                            return  co.net.une.www.gis.WSRespuestaDERSType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString18".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString18.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString100".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString100.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString14".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString14.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString15".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString15.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString10".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString10.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "WSRespuestaDE-RQ-Type".equals(typeName)){
                   
                            return  co.net.une.www.gis.WSRespuestaDERQType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString30".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString30.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString8".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString8.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString20".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString20.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString5".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString5.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString250".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString250.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString2".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString2.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString1".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString1.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "GisRespuestaGeneralType_GDE".equals(typeName)){
                   
                            return  co.net.une.www.gis.GisRespuestaGeneralType_GDE.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    